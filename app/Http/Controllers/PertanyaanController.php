<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $data = DB::table('pertanyaan')->get();

        return view('pertanyaan/index',compact('data'));
    }

    public function create(){
        return view('pertanyaan/tambah');
    }

    public function tambah(Request $request){

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request['judul'],
            "isi"   => $request['isi']
        ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil di simpan');
    }

    public function view($id){
        $data = DB::table('pertanyaan')->where('id',$id)->first();
        return view('pertanyaan.view',compact('data'));
    }

    public function edit($id){
        $data = DB::table('pertanyaan')->where('id',$id)->first();

        return view('pertanyaan/edit',compact('data'));

    }

    public function update(Request $request,$id){
        // dd($request->all());
        $query = DB::table('pertanyaan')
                ->where('id',$id)
                ->update([
                    'judul' => $request['judul'],
                    'isi'   => $request['isi']
                ]);
        return redirect('/pertanyaan')->with('update','Pertanyaan berhasil di update');;
    }

    public function delete($id){
        $query = DB::table('pertanyaan')->where('id',$id)->delete();
        return redirect('/pertanyaan')->with('delete','Pertanyaan Berhasil Di Delete');
    }
}
