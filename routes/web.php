<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/register','AuthController@register');
Route::get('/welcome','AuthController@welcome');
Route::post('/welcome','AuthController@regis_post');

//admin LTE Table
Route::get('/','HomeController@index');
Route::get('/datatable','HomeController@datatable');

//Pertanyaan
Route::get('/pertanyaan','PertanyaanController@index');
Route::get('/pertanyaan/create','PertanyaanController@create');
Route::post('/pertanyaan/tambah','PertanyaanController@tambah');
Route::get('/pertanyaan/detail/{id}','PertanyaanController@view');
Route::get('/pertanyaan/{id}/edit','PertanyaanController@edit');
Route::put('/pertanyaan/{id}','PertanyaanController@update');
Route::delete('/pertanyaan/{id}/hapus','PertanyaanController@delete');

//User
Route::resource('user', 'UserController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

