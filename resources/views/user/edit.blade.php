@extends('layouts/master')

@section('content')
<div class="card-header">
    <h3 class="card-title">Edit Pertanyaan</h3>
</div>
<form action="/pertanyaan/{{$data->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Judul Pertanyaan</label>
            <input type="text" class="form-control" id="judul" name="judul" value="{{$data->judul}}" placeholder="Masukan Judul">
        </div>
        <div class="form-group">
            <label>Isi Pertanyaan</label>
            <textarea class="form-control" id = "isi" name="isi"  rows="3" >{{$data->isi}}</textarea>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

@endsection
