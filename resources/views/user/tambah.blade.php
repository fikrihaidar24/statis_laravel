@extends('layouts/master')

@section('content')
<div class="card-header">
    <h3 class="card-title">Tambah User</h3>
</div>
<form action="/user" method="POST">
    @csrf

    <div class="card-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Nama</label>
            <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Masukan Judul">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Masukan Judul">
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection
