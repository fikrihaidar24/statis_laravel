@extends('layouts/master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">List User</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @elseif(session('update'))
        <div class="alert alert-success">
            {{session('update')}}
        </div>
        @elseif(session('delete'))
        <div class="alert alert-success">
            {{session('delete')}}
        </div>
        @endif
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Isi</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $no = 0?>
        @foreach ($data as $key =>$data)
        <?php $no++ ?>
        <tr class="text-center">
          <td>{{$key + 1}}</td>
          <td>{{$data->nama_lengkap}}
          </td>
          <td>{{$data->email}}</td>
          <td>
            <a class="btn btn-app btn-sm" href="/pertanyaan/detail/{{$data->id}}" >
                <i class="fas fa-eye"></i>
               Detail
            </a>
            <a class="btn btn-app btn-sm"
            href="/pertanyaan/{{$data->id}}/edit/">
                <i class="fas fa-edit"></i>
               Edit
            </a>
            <form action="/pertanyaan/{{$data->id}}/hapus" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" value="Hapus" class="btn btn-danger">
            </form>
          </td>
        </tr>
            @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Isi</th>
            <th>Action</th>
        </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection
