<!DOCTYPE html>
<html>
<head>
<title>SanberBook Sign Up</title>
</head>
<body>

    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <!-- Text Input -->
        <label for="fname">First name:</label><br><br>
            <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last name:</label><br><br>
            <input type="text" id="lname" name="lname"><br><br>
        <!-- Radio Input -->
        <label for="Gender">Gender:</label><br><br>
            <input type="radio" id="male" name="male" value="male">
        <label for="male">Male</label><br>
            <input type="radio" id="female" name="female" value="female">
        <label for="female">Female</label><br>
            <input type="radio" id="other" name="other" value="">
        <label for="other">Other</label><br><br>
        <!-- Select Input -->
        <label for="Nationality">Nationality:</label><br><br>

            <select name="nationality" id="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Korea">Korea</option>
                <option value="USA">USA</option>
            </select><br><br>
        <!-- Checkbox Input -->
        <label for="Language">Language Spoken:</label><br><br>
            <input type="checkbox" id="Indonesia" name="Indonesia" value="Indonesia">
        <label for="Indonesia"> Bahasa Indonesia</label><br>
            <input type="checkbox" id="English" name="English" value="English">
        <label for="English"> English</label><br>
            <input type="checkbox" id="Other" name="Other" value="Other">
        <label for="Other">Other</label><br><br>

         <!-- Text Area Input -->
        <label for="Bio">Bio:</label><br><br>
            <textarea rows="10"  cols="30" name="Bio">

            </textarea><br>
        <input type="submit" id="signup" name="signup" value="Sign Up" href="welcome.html"></input><br><br>
    </form>



</body>
</html>
